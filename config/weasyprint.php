<?php

return [
  'binary' => '/usr/local/bin/weasyprint',
  'cache_prefix' => 'weasyprint-cache_',
  'timeout' => 3600,
];
